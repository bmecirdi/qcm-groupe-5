<?php

  // var_dump($_POST);
  $sum = array_sum($_POST);
  $answer = "Tu as eu ". $sum ." bonne(s) réponse(s) sur 10.";
  $date = new DateTime();
  $filePath = 'reponse/'. date_timestamp_get($date) .'.txt';
  file_put_contents($filePath, $answer);
 ?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>Réponse</title>
  <link rel="stylesheet" href="./assets/semantic/dist/semantic.css">
</head>

<body>
  <div class="ui segment container">

    <section class="container">

      <div class="label">
        <h1 class="container__title">Ton score est de :
          <?php echo($sum*10) ?>%</h1>
      </div>
      <div class="ui teal progress" data-percent="<?php echo($sum*10) ?>" id="progressBar">
        <div class="bar"></div>
      </div>
      <br>
      <?php foreach ($_POST as $question => $value) { ?>
      <p class="container__answers">
        <?php echo str_replace('_', ' ', $question) ?>
        <?php if ($value == 1) { ?>
        <label class="container__goodAnswers" style="color:green">Bonne réponse</label>
        <?php } else { ?>
        <label class="container__badAnswers" style="color:red">Mauvaise réponse</label>
        <?php } ?>
      </p>
      <?php } ?>
      <h4 class="container__restart"><a class="container__restart--button" href="form.php">Recommencer le QCM</a></h4>
    </section>

    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
      crossorigin="anonymous"></script>
    <script src="./assets/semantic/dist/semantic.js"></script>

    <script type="text/javascript">
      $('#progressBar').progress({
        percent: $(this).data('percent')
      });
    </script>

  </div>
</body>

</html>