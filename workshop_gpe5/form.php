<?php


  # On a récupérer notre ficher
  $tableau = file_get_contents('questionnaire/questions.txt');

  # On a pu séparer par LIGNE grace à End Of Line (EOL)
  $tableau = explode(PHP_EOL, $tableau);

  $tableauQuestions = [];
  foreach ($tableau as $key => $question) {
    $question = explode('##', $question);
    array_push($tableauQuestions, $question);
  }

  # On mélange les questions
  shuffle($tableauQuestions);
  $tableauQuestions = array_slice($tableauQuestions, 0, 10); // On récupère 10 questions
?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="./styles/styles.css">
  <link rel="stylesheet" href="./styles/reset.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600" rel="stylesheet">
  <title>LearningQuiz</title>
</head>

<body>
  <section class="container">
    <img class="logo" src="./assets/images/logo.png" alt="logo">
    <h1 class="container__title">Questionnaire : </h1>
    <form class="container__form" action='reponse.php' method="POST">
      <?php foreach ($tableauQuestions as $numeroQuestion => $reponses) { // Grand tableau avec toutes les questions ?>
      <?php foreach ($reponses as $numeroReponse => $reponse) { // Question a l'unité avec réponses ?>

      <?php if ($reponses[0] == $reponse) { // On récupère la question?>
      <p class="container__question">
        Question N°
        <?php echo($numeroQuestion+1) ?><br><br>
        <?php echo($reponse); ?>
      </p>
      <?php array_shift($reponses); // On la supprime la question du tableau de réponses?>
      <?php shuffle($reponses); // On mélange les réponses ?>

      <?php foreach ($reponses as $key => $rep) { ?>
      <?php $valid = 0 ?>
      <?php if (substr($rep, 0, 1) === '(') {
                    $valid = 1;
                    $rep = trim($rep, '(');   # On supprime la première parenthèse
                    $rep = rtrim($rep, ')');  # On supprime la dernière parenthèse
                  } ?>
      <div class="test_div">
        <div class="test_div2">
          <input class="container__answers" type="radio" required name="<?php echo($reponse) ?>" value="<?php echo($valid) ?>">
          <?php echo($rep) ?><br>
        </div>
      </div>


      <?php } ?>

      <?php break; ?>
      <?php } ?>

      <?php } ?>
      <?php } ?>
      <input class="container__validation button" type="submit" value="Suivant">


    </form>

          <div class="scroll-down"></div>
    <div class="bg"></div>
  </section>

</body>

</html>