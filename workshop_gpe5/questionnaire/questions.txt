Quelle est la caractéristique de la variable quantitative ?##(Caractéristique numérique)##Caractéristique non numérique##Des variables en quantité##Des fonctions en quantité
Qu'est-ce que la boite à moustache ?##Une boite avec des moustaches à l'intérieur##(graphique résumant l'échantillon)##Un bar à chat##Un graphique représentant une forme géométrique
L'interprétation de l'écart-type et la variance est :##Plus l’écart-type et la variance sont petits, moins l’échantillon est dispersé##Moins l’écart-type et la variance sont grands, plus l’échantillon n’est pas dispersé##(Plus l’écart-type et la variance sont grands, plus l’échantillon est dispersé)##Moins l’écart-type et la variance sont petits, moins l’échantillon n’est pas dispersé
Qu'est ce que la probabilité ?##(Une fonction P assignant à tout événement A un réel P(A) entre 0 et 1)##Une fonction qui ne sert à rien##Une fonction P assignant à tout événement A un réel P(A) entre 1 et l’infini ##C’est ce qui est probable sans vraiment l’être
Quelle est la moyenne de 15-5-14-7-4-5-9-10-12 ?##81##20.25##4##(9)
Quelle est la médiane de 3 – 10 – 6 – 9 – 6 – 14 – 12 ?##7##(9)##8.60##60
Qu’est ce que l’univers en mathématiques ?##C’est l’espace##L'ensemble des chiffres et des nombres composant le monde dans lequel nous vivons##L'ensemble des calculs et des formules en mathématiques##(Concept en probabilité qui définit l’ensemble des possibilités qu’un événement peut se produire)
Qu'est-ce qu'un algorithme ?##(Ensemble de règles opératoires dont l'application permet de résoudre un problème énoncé au moyen d'un nombre fini d'opérations)##C’est une machine à café##Un système permettant de ne pas résoudre une énigme posée par un problème##Une tangente
Avec quoi peut-on traduire un algorithme ?##Grâce à un traducteur de langue tel Reverso##(Grâce à un langage de programmation, en un programme exécutable par un ordinateur)##En utilisant de manière spécifique un keylogger en soustrayant les bonnes valeurs##Grâce à un programme exécutable sur le device pour interpréter la suite logique
Qu'est-ce que l'espérance ?##C'est de toujours garder espoir##C'est ce qui caractérise le comportement moyen de la fonction. Elle est définie pour une dérivée##(C'est ce qui caractérise le comportement moyen de la variable aléatoire. Elle est définie par une variable)##C'est l'ajout de E dans une variable 
Qu'est-ce que la variance ?## C'est un nombre aléatoire##(C'est ce qui caractérise la dispersion de la variable autour de l'espérance)##C'est ce qui caractérise la dispersion de l'espérance autour de la variable## Disperser la variable en fonction de l’espérance
Quel est le symbole de pi ?##Ω##α##β##(π)
Le recensement et le sondage sont-ils différents ?##(oui)##non##peut-être##je ne sais pas
Quelle est la racine de 25 ?##25##5x5##(5)##Il n'y a pas de résultat
Quelle est l'équation d'une courbe dans un graphique ?##([yA-yB]/[xA-xB])##[xA-xB]/[yA-yB]##[xB-xA]/[yA-yB]##[xA-yA]/[xB-yB]
Quelle est la dérivée de 3x+6:##[f’]=9##(f’=3)##[f’]=0##[f’]=3x+6
Donner l’aire d’un carré:##coté+coté+coté+coté##coté^4##[Lxl]/2##([Lxl])
Quelle est la formule de Pythagore ?##BC²=AB-BC##BC²=AB+AC##(BC²=AB²+AC²)##BC²=AB²-AC²
On a 2 boules noir (N) , 2 boules rouge (R) et une boule jaune (J)##p[R]=½##p[j]=⅙##p[N]=0##(P(N)=⅖)
Quelle est l’intervalle de définition de la fonction exponentielle ?##{-∞;+∞}##({0;+∞})##{0;1}##{-∞;0}
Calculer U1 sachant que  U(n)=U0 x 5 et que U0=(-2)##(-10)##10##3##5
A quoi sert le théorème de Thalès ?##(Il sert à calculer des longueurs dans un triangle)##Il sert à calculer l'hypoténuse d'un triangle##Il sert à calculer la hauteur d’une pyramide##Il sert à préparer une pizza 
Combien de sommets a un hexagone ?##8##12##(6)##4
Que vaut 8x8 ?##(8²)##624##640##46
Que vaut -4+4+4-4 ?##(0)##4##-4##-2
Quel est ce nombre : -6 ?##(Un nombre négatif)##Un nombre décimal##Un chiffre positif##Un symbole
Qu'est-ce qui représente la fonction logarithme népérien ?##(l[x])##e[x]##m[x]##Je ne sais pas
Quelle formule peut-on utiliser pour calculer l'aire d'un cercle ?##Le cercle n'a pas d'aire, il est plat##(π x (rayon)²)##rayon²##rayon/2
Combien il y a-t-il de questions au total dans ce quiz ?##(30)##10##5##0